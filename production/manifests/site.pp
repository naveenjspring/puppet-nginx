node default {

class {'nginx::vhost':
   domain => 'training.puppet.sbose.in',
   internal_hostname => $::corporate_hostname,
   internal_hostname1 => $::networking['fqdn'],
   internal_hostname2 => $::pune_hostname,
   internal_hostname3 => $::delhi_hostname,  
 }

}
