class nginx {
package {'nginx': ensure => installed}
service { 'nginx':
 ensure => running,
 enable=> true,
 require => Package['nginx'],
}

exec { 'reload nginx':
 command => '/usr/bin/service nginx reload',
 require => Package['nginx'],
 refreshonly => true, # Execute only if some resource has changed
 }
}
